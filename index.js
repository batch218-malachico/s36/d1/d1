const express = require("express");
const mongoose = require("mongoose");

// models folder >> task.js
// controllers folder >> taskControllers.js
// route folder >> taskRoute.js
const taskRoute = require("./routes/taskRoute.js");


// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.64te6hi.mongodb.net/toDo?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now showing to port ${port}`));


