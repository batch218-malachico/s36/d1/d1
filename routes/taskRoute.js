
// This documents contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res) =>{

									// returns result from our controller
	taskController.getAllTasks().then(result => res.send(
		result));
});

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(
		result));
});

router.delete("/deleteTask/:id", (req,res) =>{
	taskController.deleteTask(req.params.id).then(result => res.send(
		result)) 	
});

router.put("/updateTask/:id", (req, res) =>{
							// will be the basis of what document we will update
							// req.body - the new document/contents
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});


router.get("/getSpecificTask/:id", (req, res) =>{

									
	taskController.getSpecificTask(req.params.id, req.body).then(result => res.send(
		result));
});

router.put("/updateStatus/:id", (req, res) =>{
							
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
});

module.exports = router;